ESTGIS suveülikool 2014
=======================
	
ESTGIS 2014. a suveülikooli_ materjalid asuvad kaustas sy2014.

Sõltuvused:: lxml_, requests_, Flask_, pyagstools_, Fiona_, Shapely_,
epydoc_

Sõltuvused on vaja eelnevalt ise paigaldada (vt viidatud www küljed,
kust saab alla laadida enda OSile ja Pythoni versioonile sobivad 
paketid).

Suveülikooli materjalid on avalikustatud BSD litsentsi alusel (vt 
sy2014/LICENSE fail).

Sõltuvused on vaja eelnevalt ise paigaldada ja nad on varustatud oma 
litsentsidega. 

Paigaldus
---------
Viimane seis otse gitist::

	git clone https://bitbucket.org/tkardi/estgis.git

või siis salvesta zip-fail::

	https://bitbucket.org/tkardi/estgis/get/master.zip
		
ja paki selle sisu lahti endale sobivasse kohta.
		
Dokumentatsioon
---------------
		
Epydoc'iga genereeritud HTML formaadis dokumentatsioon moodulite 
kohta asub sy2014/docs kaustas.	Dokumentatsiooni genereerimiseks::

	cd kataloog/kuhu/paigaldatud/sy2014
	python epydoc --config=doc.conf -v

Kasutamine
----------

Riigi Ilmateenistuse vaatlusvõrgu jaamad
________________________________________

Jaamade asukohtade salvestamiseks::

	cd kataloog/kuhu/paigaldatud/sy2014
	python ilmajaam.py

Skripti käivitudes otsitakse faili andmed/mootmised.json. Kui see
on olemas, küsitakse, kas on soov fail uuendada. Kui ei leita, siis
kas laadida andmed alla. Andmete värskendamiseks/uuendamiseks tuleb
vastata jaatavalt ::)

Riigi Ilmateenistuse vaatlusvõrgu mõõtmiste andmed kaardile
___________________________________________________________

Viimaste vaatluste andmete salvestamiseks GeoJSON 
FeatureCollectionina andmed/mootmised.json faili::

	cd kataloog/kuhu/paigaldatud/sy2014
	python ilmajaam_mootmised.py

	
Et kuvada vaatluste andmeid teenusena (jaamade asukohad
peavad olema salvestatud, siin neid enam ei salvestata)::

	cd kataloog/kuhu/paigaldatud/sy2014
	python flask_server.py
	
Seejärel on viimased automaatjaamade mõõtmised nähtavad::

	http://127.0.0.1:5000/ilmajaam/viimatine

Ja tunniandmed::

	http://127.0.0.1:5000/ilmajaam/ajalugu/<AAAA>/<KK>/<PP>/<HH24>

	
Maanteeameti Tarktee liiklusloenduste andmed kaardile
_____________________________________________________

Näite käivitamiseks::

	cd kataloog/kuhu/paigaldatud/sy2014
	python flask_server.py

Seejärel on mõõtmiste viimane seis nähtav::

	http://127.0.0.1:5000/tarktee/loendus/

Mõõtmiste mingisugust konkreetset ajahetke saab näha::

	http://127.0.0.1:5000/tarktee/loendus/<AAAA>/<KK>/<PP>/<HH24>/<MI>

Shapefailide lugemine ja kirjutamine Fionaga
____________________________________________

Näite käivitamiseks::

	cd kataloog/kuhu/paigaldatud/sy2014
	python shapes.py

See laeb Maaameti geoportaalist alla Eesti haldus- ja 
asustusjaotuse shapefailide zipid pakib need lahti andmed/ 
kataloogi. Seejärel otsib välja kõik kaustas olevad shapefailid
avab need ning otsib neist read, kus MKOOD = '0078', puhverdab
nende ruumikujusid 1000 ühikut väljapoole ja seejärel 
sissepoole kasutade shapely paketti. Puhverdatud ruumikujud 
seejärel salvestatakse samasse andmed/ kataloogi.
	
Doctestide käivitamine
______________________

Doctestide käivitamiseks::

	cd kataloog/kuhu/paigaldatud/sy2014
	python tests.py
	
.. _suveülikooli: http://www.estgis.ee/uritused/suveulikool/2014/kava/
.. _lxml: https://pypi.python.org/pypi/lxml
.. _requests: https://pypi.python.org/pypi/requests
.. _Flask: https://pypi.python.org/pypi/Flask
.. _pyagstools: https://bitbucket.org/poiss/pyagstools
.. _Fiona: https://pypi.python.org/pypi/Fiona
.. _Shapely: https://pypi.python.org/pypi/Shapely
.. _epydoc: https://pypi.python.org/pypi/epydoc