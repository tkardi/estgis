# -*- coding: utf-8 -*-
"""
@author: Tõnis Kärdi
@contact: tonis.kardi@kemit.ee
@license: BSD, vt LICENSE faili.
@require: U{fiona<https://pypi.python.org/pypi/Fiona>}
@require: U{requests<https://pypi.python.org/pypi/requests>}
@require: U{shapely<https://pypi.python.org/pypi/Shapely>}
@version: 0.1

Lihtne näide, kuidas laadida alla shapefail veebist zipina, pakkida see lahti,
lugeda ja kirjutada mingi päringu alusel uude shapefaili, vahepeal
ruumikujusid modides.

Näite käivitamiseks:

>>> import shapes

"""
from shapely.geometry import asShape
import fiona
import fnmatch
import os
import requests
import StringIO
import zipfile

ay_url = \
    'http://geoportaal.maaamet.ee/docs/haldus_asustus/asustusyksus_shp.zip'
ov_url = \
    'http://geoportaal.maaamet.ee/docs/haldus_asustus/omavalitsus_shp.zip'
mk_url = \
    'http://geoportaal.maaamet.ee/docs/haldus_asustus/maakond_shp.zip'
z_path = 'andmed'

def download_shape(url):
    """Laeb alla veebis paikneva kokkuzipitud faili.

    @param url: HTTP GET url
    @type url: C{str}
    @return: Allalaetud sisu
    @rtype: C{str}
    """
    try:
        r = requests.get(url)    
        r.raise_for_status()
        z = r.content
    except:
        raise
    finally:
        r.close()
    return z

def find(pattern, path):
    """Otsib rajalt faile mingi mustri järgi.

    @param pattern: Failinime muster, mida otsime, nt C{*.tab} leiab
        kõik .tab laiendiga failid.
    @type pattern: C{str}
    @param path: Failisüsteemi rada, millel otsime.
    @type path: C{str}
    @return: Mustriga sobivad failid sellel rajal.
    @rtype: C{list}
    """
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def unzip(zippedcontent, path):
    """Pakib zipitud sisu lahti failisüsteemi rajale.

    @param zippedcontent: veebist allalaetud zipfail.
    @type zippedcontent: C{str}
    @param path: Failisüsteemi rada, kuhu zip salvestada.
    @type path: C{str}
    """
    z = zipfile.ZipFile(StringIO.StringIO(zippedcontent))
    assert os.path.exists(path)
    z.extractall(path)
    z.close()

def getrows(filename, key, value):
    """Loeb shapefailist read ning salvestab uuede faili.

    @param filename: shapefail, mille avame ja loeme.
    @type filename: C{str}
    @param key: Loetava shapefaili atribuudi nimi, mille järgi
        hakkame ridu otsima
    @type key: C{str}
    @param value: Atribuudi väärtus, mille järgi ridu filtreerime.
    @type value: C{str} or C{int} or C{float}
    """
    print u'-- Otsin failist %s ridu, kus %s=%s ja puhverdan' %(
        filename, key, value)
    with fiona.open(filename) as _in:
        schema = _in.meta
        newfile = '%s_%s.shp' % (filename.split('.')[0], value)
        with fiona.open(newfile, 'w', **schema) as _out:
            for row in _in:
                if row['properties'][key] == value:
                    row['geometry'] = \
                        asShape(
                            row['geometry']).buffer(
                                1000).buffer(-1000).__geo_interface__               
                    _out.write(row)

def run():
    """Jooksutab terve protsessi."""
    print 'Laen andmeid:'
    for url in [ay_url, ov_url, mk_url]:
        print '-- %s' % url
        unzip(download_shape(url), z_path)
    files = find('*.shp', z_path)
    [getrows(filename, 'MKOOD', '0078') for filename in files]

if __name__ == '__main__':
    run()
