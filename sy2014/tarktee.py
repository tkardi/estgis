# -*- coding: utf-8 -*-
"""
@author: Tõnis Kärdi
@contact: tonis.kardi@kemit.ee
@license: BSD, vt LICENSE faili.
@require: U{pyagstools<https://bitbucket.org/poiss/pyagstools>}
@require: U{requests<https://pypi.python.org/pypi/requests>}
@version: 0.1

Maanteeameti Tarktee ArcGISServerilt liikluskoormuse andmete pärimine
läbi AGServeri REST API. Päringu vastus teisendatakse ESRI JSONist GeoJSONi
C{FeatureCollection}iks.

>>> import tarktee
>>> warnings = tarktee.get_warnings()
>>> assert isinstance(warnings, dict)

Kontrollime, kas saadud GeoJSON pädeb:

>>> import requests
>>> import json
>>> r = requests.post('http://geojsonlint.com/validate',
...     data=json.dumps(warnings))
>>> r.raise_for_status()
>>> response = r.json()
>>> assert response['status'] == 'ok'

"""
from pyagstools.base import FeatureSet
import json
import requests

traffic_warn_url = \
    'https://tarktee.mnt.ee/arcgis/rest/services/avalik/tarkteeandmepump/MapServer/4/query'

def open_url(url, params={}):
    """Teeb urlile HTTP GET päringu

    @param url: Päritav URL.
    @type url: C{str}
    @param params: päringu parameetrid (querystring)
    @type params: C{dict}
    @return: päringu vastus JSONis.
    @rtype: C{dict}
    """
    params['f'] = 'json'
    try:
        r = requests.get(url, params=params)
        r.raise_for_status()
        resp = r.json()
    except:
        raise
    finally:
        r.close()
    return resp

def get_warnings(ts=None):
    """Pärib Maanteeameti Tarktee Agserverilt liikluskoordmuse andmed.

    @param ts: Aeg, mille kohta andmeid päritakse
        kujul C{AAAAKKPPHH24MISS}
    @type ts: C{str}
    @return: Päringu vastus GeoJSONi C{FeatureCollection}ina.
    @rtype: C{dict}
    """
    if ts == None:
        where = 'timestampstring_end is null'
    else:
        where = \
            "'%s' between timestampstring_start and timestampstring_end" % ts
    params={'where': where,
            'outFields':'*',
            'outSR':'3301'}
    fs = FeatureSet(open_url(traffic_warn_url, params))
    return fs.to_geojson()
