# -*- coding: utf-8 -*-
"""
@author: Tõnis Kärdi
@contact: tonis.kardi@kemit.ee
@license: BSD, vt LICENSE faili.
@version: 0.1

ESTGIS suveülikool 2014 jaoks kirjutatud moodulite testid.

Kõik testid on kirjutatud doctestidena (s.t testid on kirjeldatud vastavate
moodulite ja meetodite juures docstringides).

Testide käivitamiseks käsurealt::

    cd kataloog/kuhu/paigaldatud/sy2014
    python tests.py

"""


import doctest

if __name__ == '__main__':
    doctest.testfile('ilmajaam_mootmine.py')
    doctest.testfile('ilmajaam.py')
    doctest.testfile('tarktee.py')
