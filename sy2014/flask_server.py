# -*- coding: utf-8 -*-
"""
@author: Tõnis Kärdi
@contact: tonis.kardi@kemit.ee
@license: BSD, vt LICENSE faili.
@require: U{flask<https://pypi.python.org/pypi/Flask>}
@version: 0.1

Lihtne Flaskil põhinev proxy päringute vastuvõtmiseks, edastamiseks
töötlejale ja vastuse tagastamiseks.

Kasutame seda enda kohalikul desktopil käsurealt käivitamisega:: 

    cd kataloog/kuhu/paigaldatud/sy2014
    python flask_server.py
    
"""

from flask import Flask, jsonify
import ilmajaam_mootmine
import tarktee

app = Flask(__name__)

@app.route('/')
def hello_world():
    """Hello World"""
    return 'Nobody expects the spanish inquisition!'


@app.route('/ilmajaam/viimatine')
def get_latest_ilmajaam():
    """Pärib Riigi Ilmateenistuse vaatlusvõrgu värskeimad andmed.

    @return: Päringu vastus GeoJSON C{FeatureCollection}ina.
    @rtype: C{dict}
    """
    obs = ilmajaam_mootmine.get_obs()
    return jsonify(obs)

@app.route('/ilmajaam/ajalugu/<year>/<month>/<day>/<hour>')
def get_past_ilmajaam(year, month, day, hour):
    """Pärib Riigi Ilmateenistuse vaatlusvõrgu tunniandmed.

    Sisendparameetrite korrektsust ei kontrollita, vaid saadetakse need
    edasi Riigi Ilmateenistuse tunniandmete lehele.
    
    @param year: Aasta, mille kohta andmeid päritakse kujul
        C{AAAA}.
    @param month: Kuu, mille kohta andmeid päritakse kujul
        C{KK}.
    @param day: Päev mille kohta andmeid päritakse kujul
        C{PP}.
    @param hour: 24h kella täistund, mille kohta andmeid päritakse kujul
        C{HH}.
    @return: Päringu vastus GeoJSON C{FeatureCollection}ina.
    @rtype: C{dict}
    """
    date = '%s.%s.%s' % (day, month, year)
    obs = ilmajaam_mootmine.get_past_obs(date, hour)
    return jsonify(obs)

@app.route('/tarktee/loendus/')
def get_latest_traffic_warnings():
    """Pärib Maanteeameti Tarktee serverist viimased liiklusloenduse andmed.

    @return: Päringu vastus GeoJSON C{FeatureCollection}ina.
    @rtype: C{dict}
    """
    return jsonify(tarktee.get_warnings(None))

@app.route('/tarktee/loendus/<year>/<month>/<day>/<hour>/<minute>')
def get_past_traffic_warnings(year, month, day, hour, minute):
    """Pärib Maanteeameti Tarktee serverist liiklusloenduse andmed.

    @param year: Aasta, mille kohta andmeid päritakse kujul
        C{AAAA}.
    @param month: Kuu, mille kohta andmeid päritakse kujul
        C{KK}.
    @param day: Päev mille kohta andmeid päritakse kujul
        C{PP}.
    @param hour: 24h kella täistund, mille kohta andmeid päritakse kujul
        C{HH}.
    @param hour: Kellaaja minut, mille kohta andmeid päritakse kujul
        C{MM}.
    @return: Päringu vastus GeoJSON C{FeatureCollection}ina.
    @rtype: C{dict}
    """
    ts = '%s%s%s%s%s00' %(year, month, day, hour, minute)
    return jsonify(tarktee.get_warnings(ts))

if __name__ == '__main__':
    app.run(debug=True)
